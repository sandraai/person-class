package com.sandra.person;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Person {
    private String name;
    private int height;
    private String gender;
    private String hometown;

    public Person() {

    }

    public Person(String name, int height, String gender, String hometown) {
        this.name = name;
        this.height = height;
        this.gender = gender;
        this.hometown = hometown;
    }


    public String introduceTo () {
        return "Please allow me to introduce myself, I'm a man of wealth and taste.\nI'm " + name + " from " + hometown;
    }

    public String introduceTo (Person anotherPerson) {
        if (this.hometown.equals(anotherPerson.hometown)) {
            return "I know you";
        } else {
            return this.introduceTo();
        }
    }

    public double getHeightInches () {
        BigDecimal heightInInches = new BigDecimal(height/2.54).setScale(2, RoundingMode.HALF_EVEN);
        return heightInInches.doubleValue();
    }
}


