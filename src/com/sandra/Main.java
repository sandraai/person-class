package com.sandra;
import com.sandra.person.Person;

public class Main {

    public static void main(String[] args) {
        Person unknown = new Person();
	    Person firstDude = new Person("Lelle", 180, "male", "Gusselby");
        Person secondDude = new Person("Gittan", 160, "female", "Gusselby");
        Person thirdDude = new Person("Lucifer", 190, "male", "Laxå");

        System.out.println(firstDude.getHeightInches());
        System.out.println(firstDude.introduceTo(secondDude));
        System.out.println(secondDude.introduceTo());
        System.out.println(thirdDude.introduceTo(firstDude));
    }
}
